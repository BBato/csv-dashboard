# Install dependencies only when needed
FROM node:15.12.0-alpine3.13 AS builder

RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json yarn.lock ./
COPY /frontpage/package.json /app/frontpage/package.json
COPY /server/package.json /app/server/package.json
RUN yarn install --frozen-lockfile --non-interactive

COPY /frontpage /app/frontpage
WORKDIR /app/frontpage
RUN yarn build


# Production image, copy all the files and run next
FROM node:15.12.0-alpine3.13 AS runner

ENV NODE_ENV production

WORKDIR /app/server
COPY server .

# Copy packages from previous step
COPY --from=builder /app/server/node_modules ./node_modules
COPY --from=builder /app/node_modules ./node_modules

# Copy optimized build of React application
COPY --from=builder /app/frontpage/build ./build


EXPOSE 3001
CMD ["node", "./bin/www"]
