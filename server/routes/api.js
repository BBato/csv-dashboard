var express = require("express");
var router = express.Router();
const path = require("path");
const multer = require("multer");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const neatCsv = require("neat-csv");

router.get("/", (req, res, next) => {
    res.send("OK");
});

router.get("/files", (req, res, next) => {
    let directoryData = fs.readFileSync("./uploads/index.json");
    res.send(directoryData);
});

router.get("/preview/:id/:page", async (req, res, next) => {
    const uid = String(req.params.id);
    const startingIndex = req.params.page * 15;
    const directoryData = JSON.parse(fs.readFileSync("./uploads/index.json"));
    const fileMetadata = directoryData.find((obj) => obj.id === uid);
    const fileData = fs.readFileSync(`./uploads/${fileMetadata.name}`);
    const fileJSON = await neatCsv(fileData);
    const years = fileJSON.map((obj) =>
        parseInt(obj?.date?.match(/(?:[0-9]+\/[0-9]+\/)([0-9]+)/)?.[1])
    );
    const duplicateYears = countDuplicateYears(years);
    const content = fileJSON.slice(startingIndex, startingIndex + 15);

    res.send({
        duplicateYears,
        content,
        lastPage: Math.floor(fileJSON.length / 15),
    });
});

router.get("/download/:id", async (req, res, next) => {
    const uid = String(req.params.id);
    const directoryData = JSON.parse(fs.readFileSync("./uploads/index.json"));
    const fileMetadata = directoryData.find((obj) => obj.id === uid);
    const file = `./uploads/${fileMetadata.name}`;
    res.download(file);
});

router.get("/remove/:id", async (req, res, next) => {
    const uid = String(req.params.id);
    const directoryData = JSON.parse(fs.readFileSync("./uploads/index.json"));
    const fileMetadata = directoryData.find((obj) => obj.id === uid);
    try {
        fs.unlinkSync(`./uploads/${fileMetadata.name}`);
        const newDirectoryData = directoryData.filter(
            (element) => element.name !== fileMetadata.name
        );
        fs.writeFileSync(
            "./uploads/index.json",
            JSON.stringify(newDirectoryData)
        );

        res.send({ success: true });
    } catch (err) {
        console.error(err);
        res.send({ success: false });
    }
});

function countDuplicateYears(years) {
    let counts = {};
    years.forEach(function (x) {
        counts[x] = (counts[x] || 0) + 1;
    });
    let sum = 0;
    Object.keys(counts).forEach((o) => {
        if (counts[o] > 1) sum += counts[o];
    });
    return sum;
}

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "uploads");
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});

var upload = multer({ storage: storage }).single("content");

router.post("/upload", function (req, res, next) {
    console.log(req.files);
    upload(req, res, function (err) {
        console.log(err);
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err);
        } else if (err) {
            return res.status(500).json(err);
        }
        const directoryData = JSON.parse(
            fs.readFileSync("./uploads/index.json")
        );
        const newDirectoryData = directoryData.filter(
            (element) => element.name !== req.file.filename
        );
        newDirectoryData.push({
            id: uuidv4(),
            name: req.file.filename,
            date: new Date().toUTCString(),
            data: "none",
        });
        fs.writeFileSync(
            "./uploads/index.json",
            JSON.stringify(newDirectoryData)
        );
        console.log(req.file.filename);
        return res.status(200).send(req.file);
    });
});

module.exports = router;
