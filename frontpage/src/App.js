import { createGlobalStyle, ThemeProvider } from "styled-components";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Dashboard, Preview } from "./views";
import "typeface-roboto";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;

    font-family: 'Roboto', sans-serif;
  }
`;

const theme = {
    colors: {
        primary: "#0070f3",
    },
};

function App() {
    return (
        <>
            <GlobalStyle />
            <ThemeProvider theme={theme}>
                <Router>
                    <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route exact path="/preview/:id" component={Preview} />
                    </Switch>
                </Router>
            </ThemeProvider>
        </>
    );
}

export default App;
