import React, { useState, useEffect } from "react";
import styled from "styled-components";
import css from "@styled-system/css";
import { Box, Card, Text, Button } from "../../components";
import { Layout } from "../layout";
import DataTable from "react-data-table-component";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Download } from "@styled-icons/boxicons-regular/Download";
import { RightArrowAlt } from "@styled-icons/boxicons-regular/RightArrowAlt";
import { LeftArrowAlt } from "@styled-icons/boxicons-regular/LeftArrowAlt";
import { Trash } from "@styled-icons/boxicons-regular/Trash";
import { useHistory } from "react-router-dom";

const StyledLink = styled(Box)(
    css({
        cursor: "pointer",
        "&:hover": {
            color: "gray",
        },
    })
);

export const Preview = (props) => {
    const history = useHistory();
    const baseURL =
        process.env.NODE_ENV === "development" ? `http://localhost:3001` : ``;

    const previewURL = `${baseURL}/api/preview/${props.match.params.id}`;
    const downloadURL = `${baseURL}/api/download/${props.match.params.id}`;
    const deleteURL = `${baseURL}/api/remove/${props.match.params.id}`;

    const [fileContent, setFileContent] = useState([]);
    const [columns, setColumns] = useState([]);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(0);
    const [lastPage, setLastPage] = useState(0);
    const [incrementDisabled, setIncrementDisabled] = useState(false);
    const [decrementDisabled, setDecrementDisabled] = useState(false);
    const [numDuplicateYears, setNumDuplicateYears] = useState(0);

    const getFileContent = async (pageNum) => {
        try {
            setLoading(true);

            const config = {
                method: "get",
                url: `${previewURL}/${pageNum}`,
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await axios(config);
            const fileJSON = response.data.content.map((o) => {
                return { ...o, state: o.state !== "" ? o.state : "BLANK" };
            });

            const c = Object.keys(fileJSON[0]).map((o) => {
                return { name: o, selector: o, sortable: false };
            });

            const newLastPage = response.data.lastPage;
            setIncrementDisabled(pageNum === newLastPage);
            setDecrementDisabled(pageNum === 0);
            setLastPage(newLastPage);
            setFileContent(fileJSON);
            setNumDuplicateYears(response.data.duplicateYears);
            setColumns(c);
            setLoading(false);
        } catch (e) {
            console.log(e);
        }
    };

    const onDeleteClick = async () => {
        try {
            const config = {
                method: "get",
                url: `${deleteURL}`,
                headers: {
                    "Content-Type": "application/json",
                },
            };

            await axios(config);
            history.push("/");
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        getFileContent(0);
    }, []);

    const onPageChange = (change) => {
        const newPage = page + change;

        if (newPage >= 0 && newPage <= lastPage) {
            setPage(newPage);
            getFileContent(newPage);
        }
    };

    return (
        <Layout>
            <Box py="2rem" />
            <Box
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                alignItems="center"
            >
                <StyledLink
                    display="flex"
                    onClick={() => {
                        history.push("/");
                    }}
                >
                    <Box width="2rem">
                        <LeftArrowAlt />
                    </Box>
                    <Text variant="h4" mt="0.15rem" ml="0.25rem">
                        Back
                    </Text>
                </StyledLink>

                <Box display="flex">
                    <Button mr="1rem" onClick={() => onDeleteClick()}>
                        <Box
                            display="flex"
                            flexDirection="row"
                            alignItems="center"
                        >
                            <Box width="1.25rem" mr="0.5rem" mb="0.1rem">
                                <Trash />
                            </Box>
                            Delete
                        </Box>
                    </Button>

                    <a href={downloadURL}>
                        <Button>
                            <Box
                                display="flex"
                                flexDirection="row"
                                alignItems="center"
                            >
                                <Box width="1.25rem" mr="0.5rem" mb="0.1rem">
                                    <Download />
                                </Box>
                                Download
                            </Box>
                        </Button>
                    </a>
                </Box>
            </Box>
            <Box py="2rem" />

            <Card width="100%">
                {loading ? (
                    <Box
                        py="23.25rem"
                        display="flex"
                        flexDirection="row"
                        justifyContent="center"
                    >
                        <img src="/loading.svg" />
                    </Box>
                ) : (
                    <DataTable columns={columns || []} data={fileContent} />
                )}
            </Card>
            <Box
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                pt="2rem"
            >
                <Box>
                    Number of people with the same year in the “date” field:{" "}
                    {numDuplicateYears}
                </Box>
                <Box display="flex" flexDirection="row">
                    <Button
                        mr="1rem"
                        disabled={decrementDisabled || loading}
                        onClick={() => onPageChange(-1)}
                    >
                        <Box
                            display="flex"
                            flexDirection="row"
                            alignItems="center"
                        >
                            <Box width="1.25rem" mr="0.2rem">
                                <LeftArrowAlt />
                            </Box>
                            Previous Page
                        </Box>
                    </Button>
                    <Button
                        disabled={incrementDisabled || loading}
                        onClick={() => onPageChange(1)}
                    >
                        <Box
                            display="flex"
                            flexDirection="row"
                            alignItems="center"
                        >
                            Next Page
                            <Box width="1.25rem" ml="0.2rem">
                                <RightArrowAlt />
                            </Box>
                        </Box>
                    </Button>
                </Box>
            </Box>
        </Layout>
    );
};

export default withRouter(Preview);
