import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import css from "@styled-system/css";
import { Box, Table, Card, Text, Button } from "../../components";
import { Layout } from "../layout";
import DataTable from "react-data-table-component";
import axios from "axios";
import { useHistory } from "react-router-dom";

const SubmissionInput = styled.input(
    css({
        display: "none",
    })
);

export const Dashboard = () => {
    const baseURL =
        process.env.NODE_ENV === "development" ? `http://localhost:3001` : ``;
    const submissionInputRef = useRef(null);
    const history = useHistory();
    const uploadURL = `${baseURL}/api/upload`;
    const filesURL = `${baseURL}/api/files`;
    const [files, setFiles] = useState([]);
    const [loading, setLoading] = useState(false);

    const getFiles = async () => {
        try {
            const config = {
                method: "get",
                url: filesURL,
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await axios(config);
            setFiles(response.data);
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        getFiles();
    }, []);

    const onRowClick = (state) => {
        history.push(`/preview/${state.id}`);
    };

    const columns = [
        {
            name: "File name",
            selector: "name",
            sortable: true,
        },
        {
            name: "Date Uploaded",
            selector: "date",
            sortable: true,
            right: true,
        },
    ];

    const customStyles = {
        rows: {
            style: {
                cursor: "pointer",
                "&:hover": {
                    background: "#eee",
                },
            },
        },
    };

    const handleChange = async (e) => {
        e.preventDefault();
        setLoading(true);
        const file = e.target.files[0];
        submissionInputRef.current.value = "";
        const formData = new FormData();
        formData.append("content", file);

        var config = {
            method: "post",
            url: uploadURL,
            data: formData,
        };

        try {
            await axios(config);
            await getFiles();
        } catch (e) {
            console.log(e);
        }
        setLoading(false);
    };

    return (
        <Layout>
            <Box py="2rem" />
            <Box
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                alignItems="center"
            >
                <Text variant="h3">CSV Web Application</Text>
                <Button
                    onClick={() => {
                        submissionInputRef.current.click();
                    }}
                >
                    Upload CSV
                </Button>
            </Box>
            <Box py="2rem" />

            <Card width="100%">
                {loading ? (
                    <Box
                        py="3rem"
                        display="flex"
                        flexDirection="row"
                        justifyContent="center"
                    >
                        <img src="/loading.svg" />
                    </Box>
                ) : (
                    <DataTable
                        title={files?.length ? "Available Files:" : ""}
                        columns={columns}
                        data={files}
                        onRowClicked={onRowClick}
                        customStyles={customStyles}
                    />
                )}
            </Card>

            <SubmissionInput
                type="file"
                name="file"
                required
                onChange={handleChange}
                ref={submissionInputRef}
            />
        </Layout>
    );
};
