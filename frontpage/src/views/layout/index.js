import { Box } from "../../components";

export const Layout = ({ children }) => (
    <>
        <Box display="flex" flexDirection="row" justifyContent="center">
            <Box width="80vw">{children}</Box>
        </Box>
    </>
);
