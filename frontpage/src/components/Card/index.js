import styled from "styled-components";
import { Box } from "../Box";

export const Card = styled(Box)`
    background: #ffffff;
    border: 1px solid lightgrey;
    border-radius: 10px;
    padding: 20px;


`;
