import styled from "styled-components";
import {
    space,
    layout,
    color,
    variant,
    fontSize,
    textAlign,
    typography,
} from "styled-system";

export const Text = styled.p`
    margin: 0;

    ${space} ${layout} ${color} ${textAlign} ${typography} ${variant({
        variants: {
            h1: {
                fontSize: "56px",
                fontWeight: "500",
                lineHeight: "60px",
                textShadow: "0px 4px 8px rgba(0, 0, 0, 0.2);",
                letterSpacing: "-2%",
            },
            h2: {
                fontSize: "40px",
                fontWeight: "500",
                lineHeight: "45px",
                letterSpacing: "-2%",
            },
            h3: {
                fontSize: "27px",
                fontWeight: "400",
                lineHeight: "32px",
                letterSpacing: "-2%",
            },
            h4: {
                fontSize: "21px",
                fontWeight: "500",
                lineHeight: "27px",
                letterSpacing: "-2%",
            },
            p: {
                fontSize: "27px",
                fontWeight: "300",
                lineHeight: "150%",
            },
        },
    })};
`;
