import styled from "styled-components";
import { space, layout, color, variant } from "styled-system";
import css from "@styled-system/css";

export const Button = styled("button")(
    css({
        border: "none",
        bg: '#fff',
        borderRadius: "10px",
        fontSize: "16px",
        padding: "14px 18px",
        fontWeight: 400,
        border: '1px solid lightgrey',
        "&:hover": {
            bg: "#eeeeee",
            cursor: "pointer",
        },
        "&:disabled": {
            bg: '#eee',
            cursor: "auto",

        }
    }),
    `

    -webkit-transition: background 200ms ease;
    -moz-transition: background 200ms ease;
    -ms-transition: background 200ms ease;
    -o-transition: background 200ms ease;
    transition: background 200ms ease;
  `,
    space,
    layout,
    color
);
