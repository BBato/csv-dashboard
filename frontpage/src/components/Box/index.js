import styled from 'styled-components';
import { space, layout, color, variant, flexbox, grid, position } from 'styled-system';

export const Box = styled('div')(
  { boxSizing: 'border-box' },
  space,
  layout,
  color,
  flexbox,
  grid,
  position,
  variant({
    variants: {
      flex: {
        display: 'flex',
      },
      flexGrow: {
        display: 'flex',
        flexGrow: 1,
      },
    },
  })
);
